job "fabio-job" {
  datacenters = ["dc1"]
  type = "system"
  update {
     stagger      = "60s"
     max_parallel = 1
 }
  group "fabio-group" {
    count = 1
    task "fabio-task" {
      driver = "raw_exec"
      artifact {
        source = "https://github.com/fabiolb/fabio/releases/download/v1.5.13/fabio-1.5.13-go1.13.4-linux_amd64"
      }
      config {
        command = "fabio-1.5.13-go1.13.4-linux_amd64"
      }
      resources {
        cpu    = 100 # 500 MHz
        memory = 128 # 256MB
        network {
          mbits = 100
          port "lb" {
            static = 9999
          }
          port "admin" {
            static = 9998
          }
        }
      }
    }
  }
}