from bottle import route, run, template, static_file, error


@route('/static/<file_type>/<filename>')
def server_static(file_type, filename):
    path = 'static/{}'.format(file_type)
    return static_file(filename, root=path)


@error(404)
@error(403)
def mistake(code):
    return template('There is something wrong! O_o Code: {{code}}', code=code)


@route('/')
def index():
    result = 'Hello!!!'
    return result


@route('/www')
def index01():
    result = static_file('templates/index.html', root='.')
    print(result)
    return result


run(host='localhost', port=8080, debug=True, reloader=True)
