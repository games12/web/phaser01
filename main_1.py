from starlette.applications import Starlette
from starlette.responses import PlainTextResponse
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates

app = Starlette(debug=True)
app.mount('/static', StaticFiles(directory='static'), name='static')
templates = Jinja2Templates(directory='templates')


@app.route('/')
def root(request):
    return PlainTextResponse('Hello, World!!!\n'
                             'Mail: incoming+games12-web-phaser01-17360527-issue-@incoming.gitlab.com')


@app.route('/www')
async def homepage(request):
    template = "index.html"
    context = {"request": request}
    return templates.TemplateResponse(template, context)


@app.route('/part{user_id:int}')
async def part(request):
    user_id = request.path_params['user_id']
    template = "part{}.html".format(user_id)
    context = {"request": request}
    return templates.TemplateResponse(template, context)


@app.exception_handler(404)
async def not_found(request, exc):
    """
    Return an HTTP 404 page.
    """
    return PlainTextResponse('Return an HTTP 404 page.')


@app.exception_handler(500)
async def server_error(request, exc):
    """
    Return an HTTP 500 page.
    """
    return PlainTextResponse('Return an HTTP 500 page.')


def startup():
    print('Ready to go')
