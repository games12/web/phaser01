job "phaser-job" {
  datacenters = ["dc1"]
  type = "service"
  group "phaser-group" {
    count = 3
    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }
    task "phaser-task" {
      driver = "docker"
      config {
        image = "capitan/phaser01:latest"
        port_map {
          lb = 8000
        }
      }
      resources {
        memory = 128
        network {
          port "lb" {}
        }
      }
      service {
        name = "phaser-service"
        tags = [
          "urlprefix-/"]
        port = "lb"
        check {
          name = "alive"
          type = "http"
          path = "/"
          interval = "10s"
          timeout = "2s"
        }
      }
    }
  }
}
