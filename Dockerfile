FROM python:3.8.2-slim-buster
MAINTAINER Anton <incoming+games12-web-phaser01-17360527-issue-@incoming.gitlab.com>

RUN pip install starlette hypercorn aiofiles jinja2

# create user
RUN groupadd web
RUN useradd -d /home/bottle -m bottle

#ADD main_1.py /home/bootle/main_1.py
COPY ./ /home/bottle/
WORKDIR /home/bottle

EXPOSE 8000
ENTRYPOINT ["hypercorn", "-b", "0.0.0.0:8000", "main_1:app", "--reload"]
USER bottle
