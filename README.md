# phaser01

Run project by:

Shell:
```shell script
hypercorn -b 0.0.0.0:8000  main_1:app --reload
```

Docker
```shell script
docker login docker.io
docker build --tag=capitan/phaser01:v01 .
docker build --tag=capitan/phaser01:latest .
docker push capitan/phaser01:v01
docker push capitan/phaser01:latest
docker run -p 8000:8000 capitan/phaser01:latest
```
Nomad get only latest image from hub.docker.com

Nomad:
```shell script
nomad plan phaser01.nomad
nomad run phaser01.nomad
```

